### Hi there, I'm Khalid Warsame 👋

## I'm a Flutter/Dart developer creating cross-platform mobile apps.
- 🔭 I’m currently working on my personal website and API consuming apps.
- 🌱 I’m currently learning more about UI and UX.

<br />

## Skills and Experiences:
* Flutter Mobile App Development
* Mobile App UI and UX
* API Integration
* Firebase
* Cloud Firestore
* Version Control

<br />

### Connect with me:

<p align="left">

[<img align="left" alt="khalidwar.com" width="30px" src="https://raw.githubusercontent.com/iconic/open-iconic/master/svg/globe.svg" />][website]
  
  <a href="https://www.linkedin.com/in/khalidwar/">
    <img src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" />
  </a>&nbsp;&nbsp;
  <a href="https://twitter.com/RealKhalidWar">
    <img src="https://img.shields.io/badge/twitter-%231DA1F2.svg?&style=for-the-badge&logo=twitter&logoColor=white" />
  </a>&nbsp;&nbsp;
  
</p>
 
<br />

---
<img alt="Khalid's Github Stats" src="https://github-readme-stats.vercel.app/api?username=KhalidWar&show_icons=true&count_private=true" />  [![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=KhalidWar&layout=compact)](https://github.com/anuraghazra/github-readme-stats)


[website]: https://khalidwar.com 
[twitter]: https://twitter.com/RealKhalidWar 
[linkedin]: https://linkedin.com/in/KhalidWar
